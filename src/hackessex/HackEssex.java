package hackessex;

import PV.GetIPLocation;
import PV.IPObject;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.applet.Applet;
import java.awt.Desktop;
import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class HackEssex extends Applet {

    private static final int MAX_HEAT_RADIUS = 100;

    public static final String[] TRACEROUTE_FILES = { 
        "adysoftindia.com.trace",
            "allahabadbank.in.trace",
            "adamkaly.kg.trace",
            //"test.txt", 
            "alaskazoo.org.trace",
            "ateneo.edu.trace",
            //"bad.horse.trace",
            "dveri.ua.trace",
            "frichti.com.sy.trace",
            "lcsd.gov.hk.trace",
            "lto.gov.ph.trace",
            "nasha-planeta.ru.trace",
            "parisvanjava.id.trace",
            "sman8bpp.sch.id.trace",
           "sonorestaurant.com.au.trace",
            "sss.gov.ph.trace",
            "teatroteresacarreno.gob.ve.trace",
            "uit.no.trace",
            "unrankedz.com.trace",
            "ust.edu.sd.trace",
            "www.umss.edu.bo.trace"
    };

    private static final String JSFILE = "map.js";
    private static String name;

    public static void main(String[] args) throws Exception {
        openFile(getFile());
        /*
        for(String s : TRACEROUTE_FILES){
            openFile(s);
        }
        */
    }

    public static void start(String filename) throws Exception, GeoIp2Exception {
        openFile(filename);
    }

    private static String getFile() {
        final JFileChooser fc = new JFileChooser();
        JFrame frames = new JFrame();
        frames.setSize(new Dimension(300, 400));
        int returnVal = fc.showOpenDialog(frames);

        frames.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        return fc.getSelectedFile().toString();
    }

    private static void openFile(String filename) throws FileNotFoundException, IOException, GeoIp2Exception, Exception {
        name = null;
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String fileData = new String(data, "UTF-8");
        String[] lines = fileData.split("\n");

        ArrayList<String> ips = null;
        ArrayList<ArrayList<IPObject>> wholeData = new ArrayList<ArrayList<IPObject>>();

        for (String line : lines) {
            line = line.trim();

            if (line.startsWith("traceroute")) {
                if (name == null) {
                    name = "Website address(IP) : ";
                    String[] tempAr = line.split(",");
                    name += tempAr[0].substring(13, tempAr[0].length());
                }

                if (ips != null) {
                    ArrayList<IPObject> traceroute = GetIPLocation.getIPLocations(ips);
                    wholeData.add(traceroute);
                }

                ips = new ArrayList<>();
            } else if(!line.startsWith("---") && !line.isEmpty() && 
                    !line.startsWith(" ") && !line.contains("bytes") && 
                    !line.contains("packets") && !line.startsWith("rtt")){
                String[] tempAr = line.split("\\(");
                if (!tempAr[0].contains("*")) {
                    String[] tempAr1 = tempAr[1].split("\\)");
                    if (!tempAr[0].contains("uea.ac.uk")) {
                        ips.add(tempAr1[0]);
                    }
                }
            }
        }

        //for the last traceroute
        if (ips != null) {
            ArrayList<IPObject> traceroute = GetIPLocation.getIPLocations(ips);
            wholeData.add(traceroute);
        }

        wholeData = GetIPLocation.getUniqueTracerouteList(wholeData);
//        for (ArrayList<IPObject> traceroute : wholeData) {
//            for (IPObject ip : traceroute) {
//                //System.out.println(ip.getCity());
//            }
//            System.out.println("\n\n\n\naaa");
//        }

        generateJS(wholeData);
    }

    //just hardcoded because .. no time/knowledge ;//
    private static void generateJS(ArrayList<ArrayList<IPObject>> wholeData) throws Exception {
        //clean js first
        cleanJS();

        try (FileWriter fw = new FileWriter(JSFILE, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("\nfunction init(){");

            //set header <h3> </h3>
            out.println("var element = document.getElementById(\"name\");");
            out.println("element.innerHTML = \"" + name + "\"");

            out.println("wholeData = new Array(" + wholeData.size() + ");");

            //for every traceroute
            for (int i = 0; i < wholeData.size(); i++) {
                ArrayList<IPObject> temp = wholeData.get(i);
                out.println("wholeData[" + i + "] = new Array(" + temp.size() + ");");
                out.println("wholeData[" + i + "] = [");

                //for every hop in traceroute
                for (int k = 0; k < temp.size(); k++) {
                    if (k == temp.size() - 1) {
                        out.println("{lat: " + temp.get(k).getLatitude() + ", lng: " + temp.get(k).getLongitude() + "}");
                    } else {
                        out.println("{lat: " + temp.get(k).getLatitude() + ", lng: " + temp.get(k).getLongitude() + "},");
                    }

                }
                out.println("];");
            }
            out.println("}");
        } catch (IOException e) {
            throw e;
        }

        //get data for heatMap
        generateHeatPoints();

        //set a list duh
        setList(wholeData);

        //open html
        OpenHTML("index.html");
    }

    private static void cleanJS() throws IOException {
        File file = new File(JSFILE);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String fileData = new String(data, "UTF-8");
        String[] lines = fileData.split("\n");

        for (int i = 0; i < lines.length; i++) {
            if (lines[i].startsWith("function init(){")) {
                file.delete();
                try (FileWriter fw = new FileWriter(JSFILE, true);
                        BufferedWriter bw = new BufferedWriter(fw);
                        PrintWriter out = new PrintWriter(bw)) {
                    for (int k = 0; k < i; k++) {
                        out.print(lines[k]);
                    }

                    break;

                } catch (IOException e) {
                    throw e;
                }
            }
        }

    }

    private static void OpenHTML(String filename) throws IOException {
        File htmlFile = new File(filename);
        Desktop.getDesktop().browse(htmlFile.toURI());
    }

    private static void generateHeatPoints() throws Exception {
        ArrayList<IPObject> cities = GetIPLocation.getUniqueIPObjectsListByCity();
        for (IPObject city : cities) {
            //System.out.println("miestas" + city.getCity());
            //System.out.println(city.getLatitude() + " + " + city.getLongitude());
        }

        try (FileWriter fw = new FileWriter(JSFILE, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("function getPoints(){");
            out.println("return [");

            for (IPObject city : cities) {

                for (int i = 0; i < city.getRadius(); i++) {
                    Random rand = new Random();
                    //double randomValue = rand.nextInt(MAX_HEAT_RADIUS) / 1000000;
                    double randomValue = rand.nextInt(MAX_HEAT_RADIUS);
                    randomValue = randomValue / 3000000;

                    //middle point
                    out.println("new google.maps.LatLng(" + city.getLatitude() + ", " + city.getLongitude() + "),");

                    //left top corner
                    out.println("new google.maps.LatLng(" + (Double.parseDouble(city.getLatitude()) - randomValue) + ", " + (Double.parseDouble(city.getLongitude()) - randomValue) + "),");

                    //right top corner
                    out.println("new google.maps.LatLng(" + (Double.parseDouble(city.getLatitude()) + randomValue) + ", " + (Double.parseDouble(city.getLongitude()) - randomValue) + "),");

                    //left bottom corner
                    out.println("new google.maps.LatLng(" + (Double.parseDouble(city.getLatitude()) - randomValue) + ", " + (Double.parseDouble(city.getLongitude()) + randomValue) + "),");

                    //right bottom corner
                    out.println("new google.maps.LatLng(" + (Double.parseDouble(city.getLatitude()) + randomValue) + ", " + (Double.parseDouble(city.getLongitude()) + randomValue) + "),");
                }
            }

            out.append("];}");

        } catch (Exception e) {
            throw e;
        }

    }

    private static void setList(ArrayList<ArrayList<IPObject>> wholeData) throws IOException {
        try (FileWriter fw = new FileWriter(JSFILE, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("\n\nfunction setList(){");
            out.println("var list = document.getElementById('list');");
            out.println("var valueToAdd;");
            out.println("var entry;");

            //2 default options
            out.println("entry = document.createElement('option');");
            out.println("entry.innerHTML = '<option id = \"0\">All</option>';");
            out.println("list.appendChild(entry);");

            out.println("entry = document.createElement('option');");
            out.println("entry.innerHTML = '<option id = \"1\">Heat map</option>';");
            out.println("list.appendChild(entry);");
            //for every traceroute add an element to the list
            int value = 0;
            for (ArrayList<IPObject> trace : wholeData) {
                String context = "";
                for (IPObject hop : trace) {
                    if (hop.getCity() != null && !hop.getCity().isEmpty()) {
                        context += hop.getCity() + " -> ";
                    } else {
                        context += "unknown -> ";
                    }
                }

                if (context != null) {
                    context = context.substring(0, context.length() - 3);
                    //out.println("valueToAdd = '" + context + "';");
                    out.println("entry = document.createElement('option');");
                    //out.println("entry.appendChild(document.createTextNode(valueToAdd));");
                    out.println("entry.innerHTML = '<option value = \"" + value + "\">" + context + "</option>';");
                    out.println("list.appendChild(entry);");
                    value++;
                }
            }
            out.println("}");

        } catch (Exception e) {
            throw e;
        }
    }
}
