package PV;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Location;
import static com.sun.corba.se.impl.naming.cosnaming.TransientNameServer.trace;
import eu.bitm.NominatimReverseGeocoding.Address;
import eu.bitm.NominatimReverseGeocoding.NominatimReverseGeocodingJAPI;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class GetIPLocation {

    private static ArrayList<IPObject> uniqueIPObjectsListByIP
            = new ArrayList<>();
    private static ArrayList<IPObject> uniqueIPObjectsListByCity
            = new ArrayList<>();

    public static ArrayList<IPObject> getIPLocations(ArrayList<String> ipAddresses) throws IOException, GeoIp2Exception, InterruptedException {

        File database
                = new File("Resources\\GeoLite2-City.mmdb");

        DatabaseReader reader = new DatabaseReader.Builder(database).build();

        ArrayList<IPObject> ipObjects = new ArrayList<>();

        for (String ip : ipAddresses) {
            //TimeUnit.MILLISECONDS.sleep(300);
            InetAddress ipAddress = InetAddress.getByName(ip);

            CityResponse response = reader.city(ipAddress);

            Location location = response.getLocation();
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            System.out.println(ipAddress.toString());
            String country = null;
            String city = null;

            country = response.getCountry().getName();
            city = response.getCity().getName();

            if (country == null) {
                for (IPObject ipObj : uniqueIPObjectsListByCity) {
                    if (String.valueOf(longitude).equals(ipObj.getLongitude())
                            && String.valueOf(latitude).equals(ipObj.getLatitude())) {
                        country = ipObj.getCountry();
                    }
                }
            }

            if (city == null) {
                for (IPObject ipObj : uniqueIPObjectsListByCity) {
                    if (String.valueOf(longitude).equals(ipObj.getLongitude())
                            && String.valueOf(latitude).equals(ipObj.getLatitude())) {
                        city = ipObj.getCity();
                    }
                }
            }

            if (city == null) {
                NominatimReverseGeocodingJAPI nominatim = new NominatimReverseGeocodingJAPI(18);

                Address adr = nominatim.getAdress(latitude, longitude);
                boolean valid = adr.getCity().matches("\\p{L}+");
                boolean valid2 = adr.getCity().matches("\\w+");
                //System.out.println(adr.getCity());
                if (valid && valid2) {
                    city = adr.getCity();
                }
            }

            if (country == null) {
                NominatimReverseGeocodingJAPI nominatim = new NominatimReverseGeocodingJAPI(18);

                Address adr = nominatim.getAdress(latitude, longitude);
                boolean valid = adr.getCountry().matches("\\p{L}+");
                //boolean valid2 = adr.get
                if (valid) {
                    country = adr.getCountry();
                }
            }

            if (city == null) {
                // making url request
                try {
                    //System.out.println("IR VEL");
                    TimeUnit.MILLISECONDS.sleep(300);
                    URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true");
                    // making connection
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");
                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP error code : "
                                + conn.getResponseCode());
                    }

                    // Reading data's from url
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

                    String output;
                    String out = "";
                    //System.out.println("Output from Server .... \n");
                    while ((output = br.readLine()) != null) {
                        //System.out.println(output);
                        out += output;
                    }
                    // Converting Json formatted string into JSON object
                    JSONObject json = (JSONObject) JSONSerializer.toJSON(out);
                    JSONArray results = json.getJSONArray("results");
                    JSONObject rec = results.getJSONObject(0);
                    JSONArray address_components = rec.getJSONArray("address_components");
                    for (int i = 0; i < address_components.size(); i++) {
                        JSONObject rec1 = address_components.getJSONObject(i);
                        trace(rec1.getString("long_name"));
                        JSONArray types = rec1.getJSONArray("types");
                        String comp = types.getString(0);

                        if (comp.equals("locality")) {
                            // System.out.println("city ————-" + rec1.getString("long_name"));
                            city = rec1.getString("long_name");
                        } else if (comp.equals("country")) {
                            //System.out.println("country ———-" + rec1.getString("long_name"));
                            country = rec1.getString("long_name");
                        }
                    }
                    //String formatted_address = rec.getString("formatted_address");
                    //System.out.println("formatted_address————–" + formatted_address);
                    conn.disconnect();
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (city == null) {
                    if (country != null) {
                        city = "unknown(" + country + ")";
                    }
                }

            }

            IPObject newIP = new IPObject(ip, country, city,
                    String.valueOf(longitude), String.valueOf(latitude));
            ipObjects.add(newIP);

            if (uniqueIPObjectsListByCity.size() == 0) {
                uniqueIPObjectsListByCity.add(newIP);
            } else {
                int flag = 0;
                for (IPObject ipObj : uniqueIPObjectsListByCity) {
                    if (ipObj.getCity() != null) {
                        if (ipObj.getCity().equals(newIP.getCity())) {
                            ipObj.incrementRadius();
                            flag = 1;
                        }
                    } else {
                        if (ipObj.getLatitude().equals(newIP.getLatitude())
                                && ipObj.getLongitude().equals(newIP.getLongitude())) {
                            ipObj.incrementRadius();
                            flag = 1;
                        }
                    }
                }
                if (flag == 0) {
                    uniqueIPObjectsListByCity.add(newIP);
                }
            }

        }

        return ipObjects;
    }

    public static ArrayList<IPObject> getUniqueIPObjectsListByCity() {
        ArrayList<IPObject> returnList = uniqueIPObjectsListByCity;
        uniqueIPObjectsListByCity = new ArrayList<>();
        return returnList;
    }

//    public static ArrayList<ArrayList<IPObject>> 
//        getUniqueTracerouteList(ArrayList<ArrayList<IPObject>> tracerouteList) {
//
//        ArrayList<ArrayList<IPObject>> returnList = new ArrayList<>();
//
//        for (ArrayList<IPObject> traceroute : tracerouteList) {
//            ArrayList<IPObject> newTraceroute = new ArrayList<>();
//            for (IPObject ip : traceroute) {
////                if(!newTraceroute.contains(ip)){
////                    System.out.println("aaaaaaaaaaaaaaaa");
////                    newTraceroute.add(ip);
////                }
//                if (newTraceroute.isEmpty()) {
//                    newTraceroute.add(ip);
//                } else {
//                    boolean flag = false;
//                    for (int i = 0; i < newTraceroute.size(); i++) {
//
//                        if (!ip.getLatitude().equals(newTraceroute.get(i).getLatitude())
//                                || !ip.getLongitude().equals(newTraceroute.get(i).getLongitude())) {
//                            //System.out.println("aaaaaaaaaaaaaaaaaa");
//                            flag = true;
//                        }
//                    }
//                    if(flag) newTraceroute.add(ip);
//                }
//            }
//
//            returnList.add(newTraceroute);
//        }
//
////        ArrayList<ArrayList<IPObject>> newArrayList = new ArrayList<>();
////        for(ArrayList<IPObject> traceroute : tracerouteList){
////            if(!newArrayList.contains(traceroute)){
////                newArrayList.add(traceroute);
////            }
////        }
////        
//        return returnList;
//    }
    public static ArrayList<ArrayList<IPObject>> getUniqueTracerouteList(ArrayList<ArrayList<IPObject>> tracerouteList) {

        ArrayList<ArrayList<IPObject>> returnList = new ArrayList<>();
        ArrayList<ArrayList<IPObject>> uniqueTraceroutes = new ArrayList<>();
        for (ArrayList<IPObject> traceroute : tracerouteList) {
            //traceroute = getUniqueTraceroute(traceroute);
            uniqueTraceroutes.add(getUniqueTraceroute(traceroute));
        }

//        System.out.println("PRASIDEDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//        for(ArrayList<IPObject> traceroute : tracerouteList){
//            for(IPObject obj : traceroute){
//                System.out.println(obj.getCity());
//            }
//        }
//        System.out.println("BAIGESIIIIIIIIIIIIIIIIIIIIIIIIIIII\n\n\n\n");
        //ArrayList<ArrayList<IPObject>> newArrayList = new ArrayList<>();
        for (ArrayList<IPObject> traceroute : uniqueTraceroutes) {
            if (returnList.size() == 0) {
                returnList.add(traceroute);
            } else if (!containsTraceroute(returnList, traceroute)) {
                returnList.add(traceroute);
            }
        }

        return returnList;
    }

    private static boolean containsTraceroute(ArrayList<ArrayList<IPObject>> tracerouteList, ArrayList<IPObject> traceroute) {
        int flag = 0;
        for (ArrayList<IPObject> route : tracerouteList) {
            for (IPObject att : traceroute) {
            }
            if (isTracerouteSame(route, traceroute)) {
                //flag++;
                return true;
            }
        }

//        if(flag<2) System.out.println("yes, contains, flag<2");
//        else System.out.println("no, does not contain, flag>2");
        //if(flag >= 2) return true;
        //else 
        return false;
    }

    private static boolean isTracerouteSame(ArrayList<IPObject> traceroute1,
            ArrayList<IPObject> traceroute2) {

        if (traceroute1.size() != traceroute2.size()) {
            return false;
        } else {
            for (int i = 0; i < traceroute1.size(); i++) {
                if (!traceroute1.get(i).getLatitude().equals(traceroute2.get(i).getLatitude())
                        || !traceroute1.get(i).getLongitude().equals(traceroute2.get(i).getLongitude())) {
                    //if(!traceroute1.get(i).getCity().equals(traceroute2.get(i).getCity())){
                    return false;
                }
            }
        }
        return true;
    }

    private static ArrayList<IPObject>
            getUniqueTraceroute(ArrayList<IPObject> traceroute) {

        ArrayList<IPObject> newTraceroute = new ArrayList<>();

        for (IPObject ip : traceroute) {
            if (newTraceroute.size() == 0) {
                newTraceroute.add(ip);
            } else {
                if (!containsObj(newTraceroute, ip)) {
                    newTraceroute.add(ip);
                }
            }

        }
//        System.out.println("LISTAS");
//        for(IPObject obj : newTraceroute){
//            System.out.println(obj.getCity());
//        }
//        System.out.println("BAIGESI");
        return newTraceroute;
    }

    private static boolean containsObj(ArrayList<IPObject> list, IPObject obj2) {

        for (IPObject obj1 : list) {
            if (obj1.getCity().equals(obj2.getCity())) {
                return true;
            }
        }
        return false;

    }

}
